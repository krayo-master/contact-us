# contact-us

## Build application:
mvn clean install
## Run application:
mvn spring-boot:run

## Notes:
Since I was allowed to choose a front-end framework I decided to go with simple server-side rendering approach (mvc + thymeleaf) 
instead of more popular js (react/angular) however the project can be easily transformed to REST + js client if required. 

## Possible improvements:
I believe the app contains everything necessary to fulfill the requirements but there could be some improvements if required.
(mainly to demonstrate common approaches of more robust production-ready apps, yet feels like unnecessary overkill here)

- better logging
- DTOs , mappers/converters
- better exception handling/error messages
- more tests

- retrieveAllRequestTypes() is called multiple times when the validation fails but this looks like a limitation of mvc + thymeleaf
approach, so we can perhaps try to introduce some spring/hibernate caching or go down the javascript road (or thymeleaf + htmx?)