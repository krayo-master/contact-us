package sk.krayo.contact.validation;

public class ValidationConstants {

    private ValidationConstants() {
    }

    /**
     * Regular expression for alphanumeric-only validation.
     */
    public static final String ALPHANUMERIC = "^[a-zA-Z0-9]*$";
    public static final String ALPHANUMERIC_MESSAGE = "Only alphanumeric characters allowed.";

    /**
     * Regular expression for letters-only validation.
     */
    public static final String LETTERS = "[A-Za-z ]*";
    public static final String LETTERS_MESSAGE = "Only letters allowed.";
}
