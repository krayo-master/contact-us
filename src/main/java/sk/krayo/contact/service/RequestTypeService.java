package sk.krayo.contact.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import sk.krayo.contact.persistence.model.RequestType;
import sk.krayo.contact.persistence.repository.RequestTypeRepository;

import java.util.List;

@Service
public class RequestTypeService {

    private final RequestTypeRepository repository;

    @Autowired
    public RequestTypeService(RequestTypeRepository repository) {
        this.repository = repository;
    }

    public List<RequestType> retrieveAllRequestTypes(){
        return repository.findAll();
    }

}
