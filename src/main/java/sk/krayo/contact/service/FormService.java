package sk.krayo.contact.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import sk.krayo.contact.persistence.model.Form;
import sk.krayo.contact.persistence.repository.FormRepository;

import java.util.List;

@Service
public class FormService {

    private final FormRepository formRepository;

    @Autowired
    public FormService(FormRepository formRepository) {
        this.formRepository = formRepository;
    }

    public List<Form> retrieveAllForms(){
        return formRepository.findAll();
    }

    public void saveForm(Form form){
        formRepository.save(form);
    }


}
