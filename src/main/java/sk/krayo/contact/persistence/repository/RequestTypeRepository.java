package sk.krayo.contact.persistence.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import sk.krayo.contact.persistence.model.RequestType;

@Repository
public interface RequestTypeRepository extends JpaRepository<RequestType, Long> {
}
