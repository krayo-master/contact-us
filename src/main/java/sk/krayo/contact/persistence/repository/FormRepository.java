package sk.krayo.contact.persistence.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import sk.krayo.contact.persistence.model.Form;

public interface FormRepository extends JpaRepository<Form, Long> {
}
