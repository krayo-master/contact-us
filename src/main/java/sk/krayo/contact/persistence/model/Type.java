package sk.krayo.contact.persistence.model;

public enum Type {
    CONTRACT("Contract Adjustment"),
    DAMAGE("Damage Case"),
    COMPLAINT("Complaint");

    Type(String name) {
        this.name = name;
    }

    private final String name;

    public String getName() {
        return name;
    }
}
