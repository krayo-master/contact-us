package sk.krayo.contact.persistence.model;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import lombok.Data;

@Entity
@Data
public class RequestType {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    private Type type;

    public RequestType(Type type) {
        this.type = type;
    }

    public RequestType() {

    }
}
