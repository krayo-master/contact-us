package sk.krayo.contact.persistence.model;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.Pattern;
import jakarta.validation.constraints.Size;
import lombok.Data;

import static sk.krayo.contact.validation.ValidationConstants.*;

@Entity
@Data
public class Form {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    private Type type;

    @Pattern(regexp = ALPHANUMERIC, message = ALPHANUMERIC_MESSAGE)
    @NotEmpty
    private String policyNumber;


    @Pattern(regexp = LETTERS, message = LETTERS_MESSAGE)
    @NotEmpty
    private String name;

    @Pattern(regexp = LETTERS, message = LETTERS_MESSAGE)
    @NotEmpty
    private String surname;

    @Size(max = 1000)
    @NotEmpty
    private String requestText;

}
