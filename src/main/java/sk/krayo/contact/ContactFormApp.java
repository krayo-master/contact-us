package sk.krayo.contact;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ContactFormApp {

	public static void main(String[] args) {
		SpringApplication.run(ContactFormApp.class, args);
	}

}
