package sk.krayo.contact.error;

import lombok.extern.slf4j.Slf4j;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
@Slf4j
public class ErrorControllerAdvice {


    @ExceptionHandler(Exception.class)
    public String storageException(final Exception throwable, final Model model) {
        log.error("Exception during execution of application", throwable);
        model.addAttribute("errorMessage", "Something went wrong.");
        return "error";
    }

}
