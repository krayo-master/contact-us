package sk.krayo.contact.controller;

import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;
import sk.krayo.contact.persistence.model.Form;
import sk.krayo.contact.service.FormService;
import sk.krayo.contact.service.RequestTypeService;

@Controller
public class ContactUsController {

    private final RequestTypeService requestTypeService;
    private final FormService formService;

    @Autowired
    public ContactUsController(RequestTypeService requestTypeService, FormService formService) {
        this.requestTypeService = requestTypeService;
        this.formService = formService;
    }

    @GetMapping("/")
    public ModelAndView loadContactForm(Form form, ModelMap model) {
        model.addAttribute("types", requestTypeService.retrieveAllRequestTypes());
        return new ModelAndView("contact-form", model);
    }

    @PostMapping("/")
    public String submitForm(@ModelAttribute @Valid Form form, BindingResult result, Model model) {
        if (result.hasErrors()) {
            model.addAttribute("types", requestTypeService.retrieveAllRequestTypes());
            model.addAttribute("validationError", "Some of the fields dont meet validation rules.");
            return "contact-form";
        }

        formService.saveForm(form);
        return "redirect:/success";
    }

    @GetMapping("/allForms")
    public String retrieveAllForms(final Model model) {
        model.addAttribute("forms", formService.retrieveAllForms());

        return "forms-board";
    }

    @GetMapping("/success")
    public String success() {
        return "success";
    }

}
