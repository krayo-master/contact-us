package sk.krayo.contact.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;
import sk.krayo.contact.persistence.model.RequestType;
import sk.krayo.contact.persistence.repository.RequestTypeRepository;

import java.util.List;

import static sk.krayo.contact.persistence.model.Type.*;

/**
 * A helper component to load initial data into db.
 */
@Component
public class LoadDataRunner implements CommandLineRunner {

    private final RequestTypeRepository repository;

    @Autowired
    public LoadDataRunner(RequestTypeRepository repository) {
        this.repository = repository;
    }

    @Override
    public void run(String... args) {
        final RequestType firstRequestType = new RequestType(CONTRACT);
        final RequestType secondRequestType = new RequestType(DAMAGE);
        final RequestType thirdRequestType = new RequestType(COMPLAINT);

        repository.saveAll(List.of(firstRequestType, secondRequestType, thirdRequestType));
    }

}
