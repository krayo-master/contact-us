package sk.krayo.contact;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;
import sk.krayo.contact.persistence.model.RequestType;

import java.util.List;
import java.util.stream.Stream;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static sk.krayo.contact.persistence.model.Type.*;


@SpringBootTest
@AutoConfigureMockMvc
class ContactUsControllerTest {

    private static final String TEST_STRING = "testString";

    @Autowired
    private MockMvc mockMvc;

    @Test
    void testLoadContactForm() throws Exception {
        final RequestType firstRequestType = new RequestType(CONTRACT);
        firstRequestType.setId(1L);
        final RequestType secondRequestType = new RequestType(DAMAGE);
        secondRequestType.setId(2L);
        final RequestType thirdRequestType = new RequestType(COMPLAINT);
        thirdRequestType.setId(3L);

        this.mockMvc
                .perform(get("/"))
                .andExpect(status().isOk())
                .andExpect(view().name("contact-form"))
                .andExpect(model().attribute("types", List.of(firstRequestType, secondRequestType, thirdRequestType)));
    }

    @Test
    void testSubmitFormValid() throws Exception {
        this.mockMvc
                .perform(post("/")
                        .param("type", COMPLAINT.name())
                        .param("policyNumber", TEST_STRING + "5")
                        .param("name", TEST_STRING)
                        .param("surname", TEST_STRING)
                        .param("requestText", TEST_STRING))
                .andExpect(status().is3xxRedirection())
                .andExpect(model().attributeDoesNotExist("validationError"))
                .andExpect(header().string("Location", "/success"));
    }

    @ParameterizedTest
    @MethodSource("provideInvalidFormFields")
    void testSubmitFormInvalid(String policyNumber, String name, String surname, String requestText) throws Exception {
        this.mockMvc
                .perform(post("/")
                        .param("type", DAMAGE.name())
                        .param("policyNumber", policyNumber)
                        .param("name", name)
                        .param("surname", surname)
                        .param("requestText", requestText))
                .andExpect(model().attributeExists("validationError"))
                .andExpect(view().name("contact-form"));
    }

    private static Stream<Arguments> provideInvalidFormFields() {
        return Stream.of(
                Arguments.of(TEST_STRING, TEST_STRING, TEST_STRING, null),
                Arguments.of(TEST_STRING, TEST_STRING, null, TEST_STRING),
                Arguments.of(TEST_STRING, null, TEST_STRING, TEST_STRING),
                Arguments.of(null, TEST_STRING, TEST_STRING, TEST_STRING),
                Arguments.of(".test", TEST_STRING, TEST_STRING, TEST_STRING),
                Arguments.of(TEST_STRING, "55test", TEST_STRING, TEST_STRING),
                Arguments.of(TEST_STRING, TEST_STRING, "55test", TEST_STRING),
                Arguments.of(TEST_STRING, TEST_STRING, TEST_STRING, TEST_STRING.repeat(101))
        );
    }

}
